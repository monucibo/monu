package com.app.cibopay.activities;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.app.cibopay.R;
import com.app.cibopay.databinding.ActivityForgetPassBinding;

public class ForgetPassActivity extends AppCompatActivity {
    ActivityForgetPassBinding binding;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forget_pass);
    }
}
