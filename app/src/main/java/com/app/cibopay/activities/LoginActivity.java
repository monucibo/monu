package com.app.cibopay.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.app.cibopay.R;
import com.app.cibopay.databinding.ActivityLoginBinding;
import com.app.cibopay.handler.LoginHandler;

public class LoginActivity extends AppCompatActivity {
    ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.tvName.setText("Cibo PAY");
        binding.forgtPassTxt.setOnClickListener(v->startActivity(new Intent(this,ForgetPassActivity.class)));
    }
}
